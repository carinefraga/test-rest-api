package com.ilegra.jsonplaceholder;

import com.ilegra.jsonplaceholder.model.Post;
import com.ilegra.jsonplaceholder.model.PostPatch;
import io.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.is;

public class PostTest {

    @Test
    public void createPost () {
        baseURI = "https://jsonplaceholder.typicode.com/";
        Post post2 = new Post();
        post2.setBody("Esse e um texto de exemplo de um comentario");
        post2.setTitle("Criacao do post");
        post2.setUserId(1);

        given()
                .contentType("application/json")
                .body(post2)
                .when()
                .post("/posts/")
                .then()
                .statusCode(HttpStatus.SC_CREATED)
                .body("id",is (101))
                .body("title", is(post2.getTitle()))
                .body("body", is(post2.getBody()))
                .body("userId", is(post2.getUserId()));

    }

    @Test
    public void updatePost() {
        baseURI = "https://jsonplaceholder.typicode.com/";
        Post post2 = new Post();
        post2.setBody("Esse é um exemplo de uma atualizacao de uma postagem");
        post2.setTitle("Atualizar postagem");
        post2.setUserId(1);

        given()
                .contentType("application/json")
                .body(post2)
                .when()
                .put("/posts/1")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("id",is (1))
                .body("title", is(post2.getTitle()))
                .body("body", is(post2.getBody()))
                .body("userId", is(post2.getUserId()));
    }

    @Test
    public void deletePostagem() {
        baseURI = "https://jsonplaceholder.typicode.com";

        given()

                .relaxedHTTPSValidation()
                .when()
                .delete("/posts/1")
                .then()
                .statusCode(HttpStatus.SC_OK);

    }
    @Test
    public void patchPost() {
        baseURI = "https://jsonplaceholder.typicode.com/";
        PostPatch post2 = new PostPatch();
        post2.setBody("Esse e um exemplo de uma atualizacao parcial");
        post2.setTitle("Pacth test");

        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        given()
                .contentType("application/json")
                .body(post2)
                .when()
                .patch("/posts/1")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("id",is (1))
                .body("title", is(post2.getTitle()))
                .body("body", is(post2.getBody()))
                .body("userId", is(1));
    }
    @Test
    public void getInvalidPost() {
        baseURI = "https://jsonplaceholder.typicode.com";

        given()

                .relaxedHTTPSValidation()
                .when()
                .get("/posts/101")
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND);

    }
    @Test
    public void updateInvalidPost() {
        baseURI = "https://jsonplaceholder.typicode.com/";
        Post post2 = new Post();
        post2.setBody("Esse é um exemplo de um cenario invalido");
        post2.setTitle("update invalid");
        post2.setUserId(1);

        given()
                .contentType("application/json")
                .body(post2)
                .when()
                .put("/posts/101")
                .then()
                .statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);


    }






}
