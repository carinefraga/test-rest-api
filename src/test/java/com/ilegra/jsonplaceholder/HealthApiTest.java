package com.ilegra.jsonplaceholder;

import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.is;

public class HealthApiTest {
    @Test
    public void healthApiTest() {
        baseURI = "https://jsonplaceholder.typicode.com";


        given()
                .relaxedHTTPSValidation()
                .when()
                .get("/")
                .then()
                .statusCode(HttpStatus.SC_OK);

    }


}
