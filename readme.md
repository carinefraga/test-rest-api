#Test Api Rest

Esse projeto tem por objetivo validar uma api publica, que no caso é : https://jsonplaceholder.typicode.com/

Foram implementados, os seguintes testes:

1. HealthTest: Que valida se a API está no ar, evitando esforço desnecessário em prosseguir com demais testes.
2. CreatePost: Validando a criação de um post
3. UpdatePost: Validando a atualização de um post
4. DeletePost: Validando a exclusão de um post
5. PathPost: Validando a atualização parcial de um post
6. DeleteInvalidPost: Validando a exclusão de um post inexistente, cenário negativo
7. UpdateInvalidPost: Validando a atualização de um post inexistente, cenário negativo
